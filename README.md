SharEATS
**Vue d'ensemble**

SharEATS est une application qui à pour but d’aider les personnes dans le besoin dans leur vie quotidienne, en signalant leur position sur l’application pour que l’on puisse leur apporter de la nourriture.

**Objectifs**

*Localiser les démunis* : Les utilisateurs pourront renseigner une donnée géographique pour signaler la position d’un démuni ou si il a un téléphone, le démuni pourra donner sa propre position. Il ne sera visible que par des associations.

*Se localiser comme donateur* : Un utilisateur pourra se désigner comme donateur et pourra livrer de la nourriture qu’on aura déposé sur sa localisation ou alors les personnes dans le besoin pourront venir à cet endroit pour retirer de la nourriture.

*Signalement des associations* : Les associations seront localisés et visibles par tout le monde pour que l’on puisse y déposer de la nourriture ou pour les personnes dans le besoin en retirer. Les donateurs pourront renseigner une nouvelle association.

*Suivi d’un itinéraire en temps réel* : Une personne dans le besoin pourra suivre un circuit de livraison d’une association, chaque point d’arrêt sera représenté, et le véhicule pourra être suivi en temps réel depuis l’application.

Classement des meilleurs donateurs  

*IA Reconnaissance photo* : Permet de reconnaître une personne en fonction de la photo fournie sur l’application et une photo qui sera prise pendant la complétion du formulaire pour s’assurer de la légitimité de la personne.

*Notifier une personne selon sa position* : La personne sera signalée qu’il y a une personne démunie aux alentours qui peut avoir besoin de son aide.

**Technologies utilisées**

*Frontend mobile* : React Native

*Backend* : Java Spring

*Stockage et hébergement* : Firebase et Aws

*Technologie utilisées* :MicroService,CI/CD,Terraform
